import os
import shutil
import subprocess
import sys
import time
import webbrowser

###------------------------------------------------------------------------------
###                               Useful variables
###------------------------------------------------------------------------------

# src\main\java\fr\unice\polytech\si4\client

# Needed directories
binDirectory = 'bin'
directories = [binDirectory]

# Paths
src = 'src/main/java/'
packagePath = 'fr/unice/polytech/si4/'
packageClassPath = 'fr.unice.polytech.si4.'
clientClass = 'client.Client'
serverClass = 'server.Server'
client = 'client/'
server = 'server/'
classpath = binDirectory + packagePath

# RMI
default_nb_client = 1
default_server_address = 'localhost'
default_server_port = 8080
default_client_address = 'localhost'
default_client_port = 8081

###------------------------------------------------------------------------------
###                               Script functions
###------------------------------------------------------------------------------

## Compile a java file
## Param: [javaFile:str] the name of the java file to compile
##        [binDirectory:str] the path where the compiled file will be created
##        [pathToJavaFile:list] the list of paths to the java files to compile
def compileJava(javaFile, binDirectory, pathsToJavaFile):
    print('Compiling ' + javaFile + ' ...')
    paths = ''
    for path in pathsToJavaFile:
        paths += path + javaFile + ' '
    subprocess.call('javac -d ' + binDirectory + ' ' + paths[:-1])
    print(javaFile + ' successfully compiled!\n')

## Execute a command
## Param: [command:str] the command to execute
def executeCommand(command):
    print('Executing command \'' + command + '\' ...')
    subprocess.call(command, shell=True)
    print('\'' + command + '\' successfully executed!\n')

def executeCommandInNewCmd(command):
    os.system('start cmd.exe @cmd /k \"' + command + '\"')

###------------------------------------------------------------------------------
###                             Useful functions
###------------------------------------------------------------------------------      

## Create a list of directories
## Param: [directories:list]
def createDirectories(directories):
    for directory in directories:
        if not os.path.exists(directory):
            print('Creating directory ' + directory + ' ...')
            os.makedirs(directory)
            print(directory + ' successfully created!\n')

## Remove directory from current directory to the target directory
## Param: [directory:str] the name of the directory to remove
def removeDirectory(directory):
    if (os.path.exists(directory)):
        print('Removing ' + directory + ' ...')
        shutil.rmtree(directory)
        print(directory + ' successfully removed!\n')

## Change directory
## Param: [path:str] the path to the target directory
def changeDirectory(path):
    print('Entering directory ' + path + ' ...\n')
    os.chdir(path)

def setup(nb_client, server_address, server_port, client_address, client_port):
    print('Setup:')
    print('   Number of client to listen to the server: ' + str(nb_client))
    print('   Server address: ' + server_address)
    print('   Server port: ' + str(server_port))
    print('   Client address: ' + client_address)
    print('   Client basis port: ' + str(client_port))

def loopClient(nb_client, client_address, client_port, server_port):
    for i in range(client_port, client_port + nb_client):
        executeCommandInNewCmd('java -cp ' + binDirectory + ' ' + packageClassPath + clientClass + ' ' + client_address + ' ' + str(i) + ' ' + str(server_port))

###------------------------------------------------------------------------------
###                             Script code
###------------------------------------------------------------------------------

# Remove reports directory if it already exists
removeDirectory(binDirectory)

# Create useful directories to create a clean project
createDirectories(directories)

compileJava('*.java', binDirectory, [src + packagePath + client, src + packagePath + server])

if len(sys.argv) == 1:
    setup(default_nb_client, default_server_address, default_server_port, default_client_address, default_client_port)
    executeCommandInNewCmd('java -cp ' + binDirectory + ' ' + packageClassPath + serverClass + ' ' + default_server_address + ' ' + str(default_server_port))
    loopClient(default_nb_client, default_client_address, default_client_port, default_server_port)
elif len(sys.argv) == 6:
    setup(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    executeCommandInNewCmd('java -cp ' + binDirectory + ' ' + packageClassPath + serverClass + ' ' + sys.argv[2] + ' ' + str(sys.argv[3]))
    loopClient(int(sys.argv[1]), sys.argv[4], int(sys.argv[5]), sys.argv[3])
else:
    print('Usage: If no arguments are given, the default configuration is run. Otherwise, make sure to provide the 5 needed arguments:\n')
    print('arg0: the number of client to listen to the server')
    print('arg1: the server address')
    print('arg2: the server port')
    print('arg3: the client address')
    print('arg4: the client port')

# currentDirectory = os.getcwd()
