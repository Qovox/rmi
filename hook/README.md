# Utilisation du serveur

Pour envoyer des données aux clients depuis le serveur, il faut saisir directement un message dans le serveur et appueyer sur entrée. Bien sûr, on autant de messages que l'on souhaite tant que le serveur est ouvert.

# Utilisation des scripts

## basic_script.bat

Il suffit de lancer ce script pour tester la connexion de plusieurs clients (2) au serveur. Tout se fait automatiquement (compilation + lancement du serveur puis des clients).

## script.py

En lancant ce script soit en double cliquant dessus (si jamais les fichiers d'extension .py sont liés au lanceur de programme python) soit en lancant la commande `python script.py`, c'est une configuration par défaut qui va s'exécuter.

A savoir qu'il y a juste un serveur et un client qui vont se lancer.

Sinon, il y a possibilité de lancer le programme avec des arguments pour une exécution personnalisée.

La commande serait telle que ***`python script.py nb_client server_address server_port client_address client_port_basis`*** où :
+ **nb_client** est le nombre d'instances de client à lancer *(>= 1)*.
+ **server_address** est l'adresse du serveur.
+ **server_port** est le port du serveur.
+ **client_address** est l'adresse commune pour les différents clients éventuels.
+ **client_port_basis** est le port de départ pour le premier client. Dans le cas où il y aurait justement plus d'un client, le client n se verra attribuer le port **client_port_basis + n - 1**.

# Utilisation du projet sans scripts

## Ligne de compilation

Premièrement, si le dossier bin n'existe pas, le créer au niveau de *rmi/hook*.

Avant de lancer la ligne de compilation qui suit, il faut se placer dans le dossier qui contient ce README.md, à savoir *rmi/hook* :

***javac -d bin src\main\java\fr\unice\polytech\si4\client\*.java src\main\java\fr\unice\polytech\si4\server\*.java***

## Ligne d'exécution

Exemple de lancement de serveur :

***java -cp bin fr.unice.polytech.si4.server.Server localhost 8080***

Exemple de lancement de client :

***java -cp bin fr.unice.polytech.si4.client.Client localhost 8081 8080***.

Ici le port 8081 est le port du client tandis que 8080 est le port du serveur auquel le client doit se connecter.