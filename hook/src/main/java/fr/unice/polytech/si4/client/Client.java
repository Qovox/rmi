package fr.unice.polytech.si4.client;

import fr.unice.polytech.si4.server.StreamDataNotifier;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author Olivier Boulet
 * @author Jérémy Lara
 *
 * @version 1.0
 *
 * This class instantiate the client and initialize the connection
 * with the server.
 */
public class Client {

    public static void main(String[] args) {
        String address = "localhost";
        int serverPort = 8080;
        int clientPort = 8081;

        if (args.length == 3) {
            address = args[0];
            clientPort = Integer.parseInt(args[1]);
            serverPort = Integer.parseInt(args[2]);
        }

        try {
            System.err.println("Listening for data to change...");

            Registry registry = LocateRegistry.getRegistry(address, serverPort);

            StreamDataNotifier stub = (StreamDataNotifier) registry.lookup("StreamDataNotifier");
            System.out.println(stub.connection());

            DataListener listener = new DataListenerImpl(address, clientPort);
            stub.addListener(listener);
        } catch (NotBoundException nbe) {
            System.out.println("No notifier available");
        } catch (RemoteException re) {
            System.out.println("RMI Error - " + re);
        } catch (Exception e) {
            System.out.println("Error - " + e);
        }
    }

}
