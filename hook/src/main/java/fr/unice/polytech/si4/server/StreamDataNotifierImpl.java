package fr.unice.polytech.si4.server;

import fr.unice.polytech.si4.client.DataListener;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Olivier Boulet
 * @author Jérémy Lara
 *
 * @version 1.0
 */
public class StreamDataNotifierImpl extends UnicastRemoteObject implements StreamDataNotifier {

    private String address;
    private int port;
    private List<DataListener> listeners;

    public StreamDataNotifierImpl(String address, int port) throws RemoteException {
        super(port);
        this.address = address;
        this.port = port;
        listeners = new ArrayList<>();
    }

    @Override
    public String connection() throws RemoteException {
        return "Connection established...";
    }

    @Override
    public void addListener(DataListener listener) throws RemoteException {
        System.out.println("Listener added - " + listener);
        listeners.add(listener);
    }

    @Override
    public void removeListener(DataListener listener) throws RemoteException {
        System.out.println("Listener removed - " + listener);
        listeners.remove(listener);
    }

    @Override
    public void notifyListeners(String data) throws RemoteException {

        System.err.println("Forwarding to " + listeners.size() + " listener(s)...");

        for (DataListener listener : listeners) {
            try {
                listener.dataChanged(data);
            } catch (RemoteException re) {
                System.err.println("An error has occurred.");
                removeListener(listener);
            }
        }
    }

    public String getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

}
