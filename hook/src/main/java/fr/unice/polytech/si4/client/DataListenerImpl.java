package fr.unice.polytech.si4.client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author Olivier Boulet
 * @author Jérémy Lara
 *
 * @version 1.0
 */
public class DataListenerImpl extends UnicastRemoteObject implements DataListener {

    private String address;
    private int port;
    private static int LISTENER = 0;

    public DataListenerImpl(String address, int port) throws RemoteException {
        super(port);
        this.address = address;
        this.port = port;
        LISTENER++;
    }

    @Override
    public void dataChanged(String data) {
        System.out.println(data);
    }

    public String getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "Client listener #" + LISTENER;
    }

}
