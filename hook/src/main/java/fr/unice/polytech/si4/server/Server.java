package fr.unice.polytech.si4.server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 * @author Olivier Boulet
 * @author Jérémy Lara
 *
 * @version 1.0
 *
 * This class instantiate the server and start the server.
 */
public class Server {

    public static void main(String[] args) {
        String address = "localhost";
        int port = 8080;

        if (args.length == 2) {
            address = args[0];
            port = Integer.parseInt(args[1]);
        }

        try {
            System.setProperty("java.rmi.server.hostname", address);

            StreamDataNotifier sdn = new StreamDataNotifierImpl(address, port);

            // Bind the remote object's stub in the registry
            Registry registry = LocateRegistry.createRegistry(port);
            registry.rebind("StreamDataNotifier", sdn);

            System.err.println("Server ready");

            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()) {
                String data = scanner.nextLine();
                if (data.equals("#STOP")) {
                    System.exit(0);
                } else {
                    sdn.notifyListeners(data);
                }
            }
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
