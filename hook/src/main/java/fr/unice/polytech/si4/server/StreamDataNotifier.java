package fr.unice.polytech.si4.server;

import fr.unice.polytech.si4.client.DataListener;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Olivier Boulet
 * @author Jérémy Lara
 *
 * @version 1.0
 */
public interface StreamDataNotifier extends Remote {

    String connection() throws RemoteException;

    void addListener(DataListener listener) throws RemoteException;

    void removeListener(DataListener listener) throws RemoteException;

    void notifyListeners(String data) throws RemoteException;

}
