package fr.unice.polytech.si4.client;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Olivier Boulet
 * @author Jérémy Lara
 *
 * @version 1.0
 */
public interface DataListener extends Remote {

    void dataChanged(String data) throws RemoteException;

}
